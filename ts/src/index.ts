import {
  Collection,
  Feature,
  format,
  geom,
  interaction,
  layer,
  Map,
  proj,
  render,
  source,
  style,
  View,
  MapBrowserEvent,
  extent,
} from "openlayers";

import * as proj4 from "proj4";
import {
  FeatureCollection,
  Point,
  LineStringIO,
  PropertiesIO,
} from "geojson-iots";
import { TupleType, number, string } from "io-ts";

type AnyFeature = render.Feature | Feature;
type Coordinates = [number, number];

// Type qui reprend les URLS des balades passant par un arbre,
// les coordonnées de l'arbre
// et celles de l'arbre suivant dans la balade (ou des arbres, si plusieurs balades passent par là)
type WalkLink = [string[], Coordinates, Coordinates[]];

proj.setProj4(proj4);
// from https://epsg.io/31370.proj4
proj4.defs(
  "EPSG:31370",
  "+proj=lcc +lat_1=51.16666723333333 +lat_2=49.8333339 +lat_0=90 +lon_0=4.367486666666666 +x_0=150000.013 +y_0=5400088.438 +ellps=intl +towgs84=-106.869,52.2978,-103.724,0.3366,-0.457,1.8422,-1.2747 +units=m +no_defs"
);

type Lang = "fr" | "nl" | "en";
const lang: Lang = (<any>window).woodLang;

interface Focus {
  start: string;
  end: string;
  note: string;
}

interface FocusResponse {
  focus: Focus[];
}

interface Contrib {
  created: string;
  updated: string;
  description: string;
}

interface TrRecord {
  fr: string;
  nl: string;
  en: string;
}

const records = {
  fullNameField: {
    fr: "nom_fr",
    nl: "nom_nl",
    en: "nom_en",
  },
  latinNameField: {
    fr: "latin",
    nl: "latin",
    en: "latin",
  },
  familyNameField: {
    fr: "famille_fr",
    nl: "famille_nl",
    en: "famille_en",
  },
  walkField: {
    fr: "walks_fr",
    nl: "walks_nl",
    en: "walks_en",
  },
  addressField: {
    fr: "adresse_1",
    nl: "adres_1",
    en: "__does_not_exist__",
  },
  fileName: {
    fr: "wood_file",
    nl: "wood_file",
    en: "wood_file",
  },
  toPage: {
    fr: "Voir la fiche",
    nl: "Zie pagina",
    en: "Go to page",
  },
  toWalkPage: {
    fr: "Voir la balade",
    nl: "Zie de wandeling",
    en: "See the walk",
  },
  catName: {
    fr: "category",
    nl: "category",
    en: "category",
  },
  hasPortrait: {
    fr: "has_portrait",
    nl: "has_portrait",
    en: "has_portrait",
  },
};

type RecordDB = typeof records;
type RecordKey = keyof RecordDB;

const getRecord = (k: RecordKey) => records[k][lang];

const getFromProps =
  (k: RecordKey) =>
  <T = string>(props: any, defaultValue: T) => {
    const tk = records[k][lang];
    if (props && tk in props) {
      return props[tk];
    }
    return defaultValue;
  };

const getName = getFromProps("fullNameField");
const getFamily = getFromProps("familyNameField");
const getFileName = getFromProps("fileName");
const getWalk = getFromProps("walkField");
// const getAddress = getFromProps('addressField');
// const getSpecie = getFromProps('latinNameField');
const getCatProp = getFromProps("catName");
const hasPortraitProp = getFromProps("hasPortrait");

const EPSG31370 = new proj.Projection({
  code: "EPSG:31370",
  extent: [14697.3, 22635.8, 291071.84, 246456.18],
  worldExtent: [2.5, 49.5, 6.4, 51.51],
  global: false,
  getPointResolution: (r: number) => r,
});

proj.addProjection(EPSG31370);

const wmsOrthoConfig = {
  srs: "EPSG:31370",
  params: {
    LAYERS: "Urbis:Ortho2017",
    VERSION: "1.1.1",
  },
  url: "https://geoservices-urbis.irisnet.be/geoserver/ows",
};
// const wmsOrthoConfig = {
//     srs: 'EPSG:31370',
//     params: {
//         LAYERS: 'wood-wide-web',
//         VERSION: '1.3.0',
//     },
//     url: 'http://127.0.0.1:2000/wms'
//     // url: 'http://172.17.0.1:8080/'
// };

const jsonFormat = new format.GeoJSON();

const isRenderFeature = (a: AnyFeature): a is render.Feature => {
  return "getExtent" in a;
};

// const getProp =
//     (k: string) =>
//         (f: AnyFeature) => {
//             const props = f.getProperties();
//             return k in props ? props[k] : null;
//         }

// const checkProp =
//     <T>(k: string, v: T) =>
//         (f: AnyFeature) =>
//             getProp(k)(f) === v;

const bufferFeature = (f: AnyFeature) => (radius: number) => {
  if (isRenderFeature(f)) {
    return new geom.Circle(extent.getCenter(f.getExtent()), radius);
  }
  return new geom.Circle(
    (f.getGeometry() as geom.Point).getCoordinates(),
    radius
  );
};

const lineFeature = (f: AnyFeature) => {
  const coords = isRenderFeature(f)
    ? extent.getCenter(f.getExtent())
    : (f.getGeometry() as geom.Point).getCoordinates();
  return new geom.MultiLineString([
    [
      [coords[0], 0],
      [coords[0], coords[1] - 5],
    ],
    [
      [0, coords[1]],
      [coords[0], coords[1] - 5],
    ],
  ]);
};

const dist = (p0: Coordinates, p1: Coordinates) => {
  const dx = p1[0] - p0[0];
  const dy = p1[1] - p0[1];
  return Math.sqrt(dx * dx + dy * dy);
};

const add = (v1: Coordinates, v2: Coordinates, n: number): Coordinates => {
  const d = dist(v1, v2);
  const t = n / d;
  const rx = v1[0] + (v2[0] - v1[0]) * t;
  const ry = v1[1] + (v2[1] - v1[1]) * t;
  return [rx, ry];
};

const perp = (a: Coordinates, b: Coordinates): Coordinates => {
  const c = [b[0] - a[0], b[1] - a[1]];
  const p = [c[1], -c[0]];
  return [p[0] + a[0], p[1] + a[1]];
};

const { forward } = proj4("EPSG:4326", "EPSG:31370");
const toLambert72 = (c: Coordinates): Coordinates => forward(c) as Coordinates;

// const arrowFeature =
//     (f: AnyFeature) =>
//         (radius: number) => {
//             const walks = getFromProps('walkField')(f.getProperties(), [] as WalkLink[]);
//             const start = toLambert72(walks[1]);
//             const end = toLambert72(walks[2][0]); //Ne prend en compte que la 1ère balade passant par là
//             const d = dist(start, end)*0.75;

//             const a = add(start, end, d*0.95);
//             const b = add(start, end, d);

//             const stem = [start, b];
//             const p0 = perp(a, b);
//             const p1 = add(a, p0, -dist(a, p0));

//             return (new geom.MultiLineString([
//                 stem,
//                 [p0, b],
//                 [p1, b],
//             ]));
//     }

const strokeStyle = (width: number, color: string): style.Stroke =>
  new style.Stroke({
    width,
    color,
  });

const baseStyleWidth = 7;
const baseStyleRadius = 7;
const highlightWidth = 8;
const highlightRadius = 14;

const baseStyle = (color: string) => (f: AnyFeature) =>
  new style.Style({
    stroke: strokeStyle(baseStyleWidth, color),
    geometry: bufferFeature(f)(baseStyleRadius),
  });

const focusStyle = (f: AnyFeature) => [
  new style.Style({
    stroke: strokeStyle(1, "white"),
    geometry: lineFeature(f),
  }),
  new style.Style({
    stroke: strokeStyle(baseStyleWidth, "#d6ca55"),
    geometry: bufferFeature(f)(baseStyleRadius + 10),
  }),
];

const inventoryStyle = (f: AnyFeature) =>
  new style.Style({
    // stroke: strokeStyle(2, "#97cfb1"),
    stroke: strokeStyle(2, "#32d148"),

    geometry: bufferFeature(f)(1),
  });

const walkStyle = (f: AnyFeature) => [
  new style.Style({
    stroke: strokeStyle(2, "#f4841cc5"),
  }),
  new style.Style({
    stroke: strokeStyle(baseStyleWidth, "#f4841cc5"),
    geometry: bufferFeature(f)(4),
  }),
];

const pathStyle = (f: AnyFeature) =>
  new style.Style({
    stroke: strokeStyle(1.5, "#f4841cc5"),
  });

const avecPortraitStyle = (f: AnyFeature) => [
  new style.Style({
    // stroke: strokeStyle(baseStyleWidth, '#97cfb1'),
    stroke: strokeStyle(baseStyleWidth + 3, "#32d148"),
    geometry: bufferFeature(f)(baseStyleRadius + 3),
  }),
  new style.Style({
    stroke: strokeStyle(baseStyleWidth, "white"),
    geometry: bufferFeature(f)(baseStyleRadius),
  }),
];

//test if a Point is in a walk
const walkable =
  (s: (f: AnyFeature) => style.Style | style.Style[]) => (f: AnyFeature) => {
    const initial = s(f);
    const walksBy = getFromProps("walkField")(
      f.getProperties(),
      [] as WalkLink[]
    );
    const isWalkable = walksBy[0].length > 0 && walksBy[2].length > 1;

    if (isWalkable) {
      if (Array.isArray(initial)) {
        return initial.concat(walkStyle(f));
      }
      return [initial, walkStyle(f)[0], walkStyle(f)[1]];
    }
    return initial;
  };

const Styles = {
  // 'rmqb': walkable(baseStyle('#97cfb1')),
  rmqb: walkable(baseStyle("#32d148")),
  rmqe: walkable(baseStyle("#ead341")),
  // 'plnt': walkable(baseStyle('#cd9351')),
  plnt: walkable(baseStyle("#97cfb1")),

  // 'adopte': baseStyle('#32d148'),
  ctrb: walkable(baseStyle("#97cfb1")),
  adpt: walkable(baseStyle("#97cfb1")),
  remarquableAvecPortrait: walkable(avecPortraitStyle),
  focus: focusStyle,
  inv: walkable(inventoryStyle),

  path: pathStyle,
};

type StyleMap = typeof Styles;
type StyleKey = keyof StyleMap;

const getCategory = (f: AnyFeature | Feature) => {
  const props = f.getProperties();
  const h: boolean = props["highlight"];
  const cat: StyleKey = props["category"];
  if (h) {
    return "focus";
  }
  if ("rmqb" === cat && "has_portrait" in props && props["has_portrait"]) {
    return "remarquableAvecPortrait";
  }
  return cat;
};

const makeDefaultStyle = () => (f: AnyFeature) => {
  const cat = getCategory(f);
  const s = Styles[cat];
  return s(f);
};

const makeHighlightStyle = () => (f: AnyFeature) => {
  const cat = getCategory(f);
  const catStyle = Styles[cat](f);
  const selectStyle =
    cat === "inv"
      ? [
          new style.Style({
            stroke: strokeStyle(highlightWidth, "white"),
            geometry: bufferFeature(f)(highlightRadius),
          }),
        ]
      : [
          new style.Style({
            stroke: strokeStyle(highlightWidth + 2, "white"),
            geometry: bufferFeature(f)(highlightRadius + 2),
          }),
        ];
  if (Array.isArray(catStyle)) {
    return catStyle.concat(selectStyle);
  }
  return [catStyle].concat(selectStyle);
};

const makeSelectStyle = () => (f: AnyFeature) => {
  const catStyle = Styles[getCategory(f)](f);
  const selectStyle = [
    new style.Style({
      stroke: strokeStyle(highlightWidth, "white"),
      geometry: bufferFeature(f)(highlightRadius),
    }),
    new style.Style({
      stroke: strokeStyle(highlightWidth / 2, "grey"),
      geometry: bufferFeature(f)(highlightRadius + highlightWidth),
    }),
  ];
  if (Array.isArray(catStyle)) {
    return catStyle.concat(selectStyle);
  }
  return [catStyle].concat(selectStyle);
};

const boxedText = (t: string, className: string) => {
  const e = document.createElement("div");
  e.setAttribute("class", className);
  e.appendChild(document.createTextNode(t));
  return e;
};

const boxedAnchor = (t: string, href: string, className: string) => {
  const e = document.createElement("div");
  const a = document.createElement("a");
  e.setAttribute("class", className);
  a.setAttribute("href", href);
  a.appendChild(document.createTextNode(t));
  e.appendChild(a);
  return e;
};

const makeImage = (id: string, cat: StyleKey) => {
  const w = document.createElement("div");
  const l = document.createElement("div");
  const i = document.createElement("img");
  w.appendChild(l);
  w.appendChild(i);

  l.appendChild(document.createTextNode(`Loading image for ${id}`));
  i.addEventListener("load", (_e) => w.removeChild(l));
  i.addEventListener("error", (_e) => {
    w.removeChild(l);
    w.removeChild(i);
  });
  // i.setAttribute('src', `/media/tree/${id}/600`)
  if (cat === "adpt") {
    i.setAttribute("src", `/media/contrib/${id}/600`);
  } else {
    i.setAttribute("src", `/media/tree/${id}/600`);
  }
  return w;
};

const makePopup = (f: Feature, props: any) => {
  const element = document.createElement("div");
  const links = document.createElement("div");
  const cat = getCategory(f);
  element.setAttribute("class", "popup");
  element.id = props.id;

  var title = "";
  if (getName(props, "")) {
    title = getName(props, "");
  } else {
    title = getFamily(props, "");
  }
  element.appendChild(boxedText(title, "name"));
  element.appendChild(links);
  element.appendChild(makeImage(element.id, cat));

  if (cat === "focus") {
    const focusList = document.createElement("div");
    focusList.setAttribute("class", "focus-list");
    element.appendChild(focusList);

    fetch(`/${lang}/focus/${props.id}`)
      .then((response) => response.json())
      .then((data: FocusResponse) => {
        data.focus.forEach((f) => {
          const fe = document.createElement("div");
          fe.setAttribute("class", "focus-item");
          fe.appendChild(document.createTextNode(f.note));
          focusList.appendChild(fe);
        });
      });
  }

  // if (cat === 'ctrb') {
  //     const contrib = document.createElement('div')
  //     contrib.setAttribute('class', 'contrib')
  //     element.appendChild(contrib);
  /* issue 76
        fetch(`/adopte/${props.id}`)
            .then(response => response.json())
            .then((data: Contrib) => {
                adopte.appendChild(document.createTextNode(data.description))
            });
        */
  // }

  links.setAttribute("class", "links");

  links.appendChild(
    boxedAnchor(getRecord("toPage"), getFileName(props, ""), "link")
  );

  // Link to walks
  const walks = getFromProps("walkField")(f.getProperties(), [] as WalkLink[]);
  if (walks && walks[0] && walks[0].length > 0) {
    links.appendChild(
      boxedAnchor(
        // ATTENTION, doesn't work if many walks through one point
        getRecord("toWalkPage"),
        walks[0][0],
        "link"
      )
    );
  }

  return element;
};

const makeLabel = (_f: Feature | render.Feature, props: any) => {
  const element = document.createElement("div");
  element.setAttribute("class", "label");
  element.id = props.id;
  var title = "";
  if (getName(props, "")) {
    title = getName(props, "");
  } else {
    title = getFamily(props, "");
  }

  element.appendChild(boxedText(title, "name"));
  return element;
};

const removeElement = (selector: string) => () => {
  const preview = document.querySelector(selector);
  if (preview) {
    let c = preview.firstChild;
    while (c) {
      preview.removeChild(c);
      c = preview.firstChild;
    }
  }
};

const addElement = (selector: string) => (e: HTMLElement) => {
  const preview = document.querySelector(selector);
  if (preview) {
    preview.appendChild(e);
  }
};

const removeOverlay = removeElement(".tree-preview");
const addOverlay = addElement(".tree-preview");

const removeLabel = removeElement(".tree-label");
const addLabel = addElement(".tree-label");

const makeMap = () => {
  // const target = document.getElementById('map')
  const view = new View({
    projection: EPSG31370,
    center: [148651, 170897],
    rotation: 0,
    zoom: 6,
  });
  const map = new Map({
    target: "map",
    view,
  });

  const layers: layer.Vector[] = [];
  const baseLayer = new layer.Tile({
    source: new source.TileWMS({
      projection: proj.get(wmsOrthoConfig.srs),
      params: {
        ...wmsOrthoConfig.params,
        TILED: true,
      },
      url: wmsOrthoConfig.url,
    }),
  });

  map.addLayer(baseLayer);

  let popup: HTMLElement | null = null;
  let label: HTMLElement | null = null;
  let lastMove = performance.now();

  map.on("pointermove", (e: Partial<MapBrowserEvent>) => {
    const t = performance.now();
    if (t - lastMove < 100) {
      return;
    }
    lastMove = t;
    const pixel: [number, number] = e.pixel !== undefined ? e.pixel : [0, 0];
    const fs = map.getFeaturesAtPixel(pixel);
    if (fs && fs.length > 0) {
      const props = fs[0].getProperties();
      if (props) {
        // console.log(Object.keys(props))
        if (label && label.id !== props.id) {
          removeLabel();
          label = null;
        }
        if (label === null) {
          label = makeLabel(fs[0], props);
          addLabel(label);
        }
      }
    } else if (label !== null) {
      removeLabel();
      label = null;
    }
  });

  const select = new interaction.Select({
    multi: true,
    style: makeSelectStyle(),
  });

  select.on("select", (e: any) => {
    const fs: Collection<Feature> = e.target.getFeatures();
    if (fs.getLength() === 0) {
      removeOverlay();
      popup = null;
      return;
    }

    type Nothing = "Nothing";
    type OnlyOneSelectable = "OnlyOneSelectable";
    type OnlyManySelectable = "OnlyManySelectable";
    type OnlyOneNonSelectable = "OnlyOneNonSelectable";
    type OnlyManyNonSelectable = "OnlyManyNonSelectable";
    type MixedOneSelectable = "MixedOneSelectable";
    type MixedManySelectable = "MixedManySelectable";

    type Case =
      | Nothing
      | OnlyOneSelectable
      | OnlyManySelectable
      | OnlyOneNonSelectable
      | OnlyManyNonSelectable
      | MixedOneSelectable
      | MixedManySelectable;

    const selectOnlyOneSelectable = (_s: Nothing): Case => "OnlyOneSelectable";
    const selectOnlyOneNonSelectable = (_s: Nothing): Case =>
      "OnlyOneNonSelectable";
    const selectOnlyManySelectable = (
      _s: OnlyOneSelectable | OnlyManySelectable
    ): Case => "OnlyManySelectable";
    const selectOnlyManyNonSelectable = (
      _s: OnlyOneNonSelectable | OnlyManyNonSelectable
    ): Case => "OnlyManyNonSelectable";
    const selectMixedOneSelectable = (
      _s:
        | OnlyOneNonSelectable
        | OnlyManyNonSelectable
        | OnlyOneSelectable
        | MixedOneSelectable
    ): Case => "MixedOneSelectable";
    const selectMixedManySelectable = (
      _s: MixedOneSelectable | MixedManySelectable | OnlyManySelectable
    ): Case => "MixedManySelectable";

    const isNonSelectable = (f: Feature) => {
      const props = f.getProperties();
      // return (props.category && (props.category === 'inv' || props.category === 'path'));
      return props.category && props.category === "path";
    };

    const selState = fs.getArray().reduce((acc, f) => {
      if (isNonSelectable(f)) {
        switch (acc) {
          case "Nothing":
            return selectOnlyOneNonSelectable(acc);
          case "OnlyOneSelectable":
            return selectMixedOneSelectable(acc);
          case "OnlyManySelectable":
            return selectMixedManySelectable(acc);
          case "OnlyOneNonSelectable":
            return selectOnlyManyNonSelectable(acc);
          case "OnlyManyNonSelectable":
            return selectOnlyManyNonSelectable(acc);
          case "MixedOneSelectable":
            return selectMixedOneSelectable(acc);
          case "MixedManySelectable":
            return selectMixedManySelectable(acc);
        }
      }
      switch (acc) {
        case "Nothing":
          return selectOnlyOneSelectable(acc);
        case "OnlyOneSelectable":
          return selectOnlyManySelectable(acc);
        case "OnlyManySelectable":
          return selectOnlyManySelectable(acc);
        case "OnlyOneNonSelectable":
          return selectMixedOneSelectable(acc);
        case "OnlyManyNonSelectable":
          return selectMixedOneSelectable(acc);
        case "MixedOneSelectable":
          return selectMixedManySelectable(acc);
        case "MixedManySelectable":
          return selectMixedManySelectable(acc);
      }
    }, "Nothing" as Case);

    console.log(selState);

    const rfs = fs.getArray().filter((f) => {
      // const props = f.getProperties();
      // const isInventory = props.category && props.category === 'inventory';
      // return !isInventory;
      return !isNonSelectable(f);
    });
    const ifs = fs.getArray();
    // .filter(f => {
    // const props = f.getProperties();
    // const isInventory = props.category && props.category === 'inv';
    // return isInventory;
    //})

    const withFirst = (f: (f: Feature) => void) => {
      rfs.forEach((ft, idx) => {
        if (0 === idx) {
          f(ft);
        } else {
          fs.remove(ft);
        }
      });
    };

    const clearSelectNonSelectable = (
      _s: MixedOneSelectable | MixedManySelectable
    ) => ifs.forEach((f) => fs.remove(f));
    const clearSelectAll = (
      _s: OnlyOneNonSelectable | OnlyManyNonSelectable
    ) => {
      fs.clear();
      removeOverlay();
      popup = null;
    };
    const showFirstSelectable = (
      _s:
        | MixedOneSelectable
        | MixedManySelectable
        | OnlyOneSelectable
        | OnlyManySelectable
    ) =>
      withFirst((f) => {
        const props = f.getProperties();
        if (props) {
          if (popup && popup.id !== props.id) {
            removeOverlay();
            popup = null;
          }
          if (popup === null) {
            popup = makePopup(f, props);
            addOverlay(popup);
            window.setTimeout(() => map.updateSize(), 100);
          }
        }
      });

    switch (selState) {
      case "Nothing":
        return;
      case "OnlyOneSelectable":
        return showFirstSelectable(selState);
      case "OnlyManySelectable":
        return showFirstSelectable(selState);
      case "OnlyOneNonSelectable":
        return clearSelectAll(selState);
      case "OnlyManyNonSelectable":
        return clearSelectAll(selState);
      case "MixedOneSelectable":
        showFirstSelectable(selState);
        return clearSelectNonSelectable(selState);
      case "MixedManySelectable":
        showFirstSelectable(selState);
        return clearSelectNonSelectable(selState);
    }
  });

  map.addInteraction(select);

  const addLayer = (name: string, features: Feature[]) => {
    const woodSource = new source.Vector();
    const woodLayer = new layer.Vector({
      source: woodSource,
      style: makeDefaultStyle(),
    });
    woodLayer.set("name", name);
    layers.push(woodLayer);
    woodSource.addFeatures(features);
    map.addLayer(woodLayer);
  };

  const visible = (layerNames: string[]) =>
    layers.forEach((l) => l.setVisible(layerNames.indexOf(l.get("name")) >= 0));

  const highlight = (layerNames: string[]) => {
    layers.forEach((l) => l.setStyle(makeDefaultStyle()));
    layers.forEach((l) => {
      const name = l.get("name");
      if (layerNames.indexOf(name) >= 0) {
        console.log(`highlight ${name} ${layerNames}`);
        l.setStyle(makeHighlightStyle());
      }
    });
  };
  const selectLayer = (layerNames: string[]) => {
    layers.forEach((l) => {
      l.setStyle(
        new style.Style({
          stroke: strokeStyle(0, "#32d100"),
        })
      );
    });

    layers.forEach((l) => {
      const name = l.get("name");
      if (layerNames.indexOf(name) >= 0) {
        l.setStyle(makeDefaultStyle());
      }
    });
  };
  const allLayers = () => {
    layers.forEach((l) => l.setStyle(makeDefaultStyle()));
  };

  // see https://gitlab.com/atelier-cartographique/wood-site/issues/114

  window.addEventListener("resize", () =>
    window.setTimeout(() => map.updateSize(), 100)
  );

  return {
    addLayer,
    visible,
    highlight,
    selectLayer,
    allLayers,
    map,
  };
};

type ToggleListener = (a: boolean) => void;

class Toggler {
  private value = false;
  private listeners: ToggleListener[] = [];

  constructor(private name: string) {}

  toggle() {
    this.value = !this.value;
    this.listeners.forEach((f) => f(this.value));
    return this.value;
  }

  concat(xs: string[]) {
    if (this.value) {
      return xs.concat([this.name]);
    }
    return xs;
  }

  onToggle(cb: ToggleListener) {
    this.listeners.push(cb);
  }
}

//Species legend items highlighting
const makeFilterSpecies = (name: string) => {
  const node = document.createElement("div");
  node.setAttribute("class", "legend-item");
  const toggle = new Toggler(name);
  const reset = true;
  var togState: boolean;

  node.innerText = name;
  node.addEventListener(
    "click",
    () => {
      togState = toggle.toggle();
      if (togState) {
        node.setAttribute("class", "legend-item highlight");
      } else {
        node.setAttribute("class", "legend-item");
      }
    },
    false
  );

  const onlySpecies = <HTMLElement>document.getElementById("only-species");
  onlySpecies.addEventListener("click", () => {
    node.setAttribute("class", "legend-item");
    if (togState) {
      toggle.toggle();
    }
  });

  return { node, toggle };
};

const start = () => {
  const { addLayer, highlight, selectLayer, allLayers, map } = makeMap();
  const speciesSet = new Set<string>();
  const catArray = new Array<string>("inv", "ctrb", "plnt", "rmqe", "rmqb");
  const loaders = document.querySelectorAll(".data-loader");
  loaders.forEach((e) => e.classList.replace("none", "loading"));

  const maybeSelectLayer = () => {
    try {
      const loc = document.location;
      const params = new URL(loc.href).searchParams;
      const category = params.get("cat");
      if (category != null) {
        selectLayer([category]);
      }
    } catch (e) {
      console.log(e);
    }
  };
  const loadRoute = maybeSelectLayer;

  fetch("/atlas/data/")
    .then((response) => {
      console.log(`--------- fetch atlas-data`);
      const data = response.json();
      return data;
    })
    .then((geoData: FeatureCollection) => {
      console.log(`fetch atlas-data FeatureCollection`);

      //species layers
      geoData.features
        .map((f) => getFamily(f.properties, ""))
        .filter((name) => name !== "")
        .forEach((name) => speciesSet.add(name));
      const species = Array.from(speciesSet.values()).sort();

      species.forEach((name) =>
        addLayer(
          name,
          geoData.features
            .filter((f) => getFamily(f.properties, "") === name)
            .map((f) => jsonFormat.readFeature(f))
        )
      );

      const filterElem = document.getElementById("specie-filter");
      const toggles: Toggler[] = [];
      const toggleHandler = () =>
        highlight(toggles.reduce((acc, t) => t.concat(acc), [] as string[]));

      console.log(`fetch atlas-data FeatureCollection 2222`);

      if (filterElem) {
        species.forEach((name) => {
          const { node, toggle } = makeFilterSpecies(name);
          toggle.onToggle(toggleHandler);
          toggles.push(toggle);
          filterElem.appendChild(node);
        });
      }

      loaders.forEach((e) => e.classList.replace("loading", "loaded"));

      //categories layers
      //html legend elements
      const filterTreesArray = Array.from(
        document.getElementsByClassName("label")
      );
      //content legend elements
      const catLegend = filterTreesArray.map((e) => <string>e.textContent);
      //layer name
      const catFromLegend = (legendName: string) => {
        switch (legendName) {
          case catLegend[0]:
            return ["remarquableAvecPortrait"];
            break;
          case catLegend[1]:
            return ["rmqb"];
            break;
          case catLegend[2]:
            return ["walk"];
            break;
          case catLegend[3]:
            return ["rmqe"];
            break;
          case catLegend[4]:
            return ["plnt", "ctrb", "adpt"];
            break;
          case catLegend[5]:
            return ["inv"];
            break;
          default:
            return [];
        }
      };

      addLayer(
        "remarquableAvecPortrait",
        geoData.features
          .filter((f) => hasPortraitProp(f.properties, "") === true)
          .map((f) => jsonFormat.readFeature(f))
      );
      catArray.forEach((name) => {
        addLayer(
          name,
          geoData.features
            .filter((f) => getCatProp(f.properties, "") === name)
            .map((f) => jsonFormat.readFeature(f))
        );
      });
      addLayer(
        "walk",
        geoData.features
          .filter((f) => getWalk(f.properties, "")[0].length > 1)
          .map((f) => jsonFormat.readFeature(f))
      );

      filterTreesArray.forEach((node) => {
        const legendName = <string>node.textContent;
        node.addEventListener(
          "click",
          () => {
            const allTreesButton = <HTMLElement>(
              document.getElementById("all-trees")
            );
            allTreesButton.setAttribute("class", "all-trees");

            const catName = catFromLegend(legendName);
            selectLayer(catName);

            filterTreesArray.forEach((node2) => {
              if (node2 == node) {
                node2.setAttribute("class", "label selected");
              } else {
                node2.setAttribute("class", "label");
              }
            });
            console.log("click!");
          },
          false
        );
      });
      const resetButtons = Array.from(
        document.getElementsByClassName("all-trees")
      );
      resetButtons.forEach((b) => {
        b.addEventListener("click", () => {
          b.setAttribute("class", "all-trees hidden");
          allLayers();
        });
      });

      // Buttons to go from one legend to the other
      const onlySpecies = <HTMLElement>document.getElementById("only-species");
      const onlyCategories = <HTMLElement>(
        document.getElementById("only-categories")
      );

      const categoriesNode = <HTMLElement>(
        document.getElementById("legend-categories")
      );
      const speciesNode = <HTMLElement>(
        document.getElementById("legend-species")
      );
      // const speciesLegendItems = Array.from(document.getElementsByClassName("legend-item"));

      onlySpecies.addEventListener("click", () => {
        categoriesNode.setAttribute("class", "legend-map hidden");
        speciesNode.setAttribute("class", "specie-filter-wrapper");
        onlyCategories.setAttribute("class", "all-trees");
      });
      onlyCategories.addEventListener("click", () => {
        categoriesNode.setAttribute("class", "legend-map");
        speciesNode.setAttribute("class", "specie-filter-wrapper hidden");
        onlySpecies.setAttribute("class", "all-trees");
      });

      loaders.forEach((e) => e.classList.replace("loading", "loaded"));
    })
    .then(() => fetch("/walk/paths/"))
    .then((response) => response.json())
    .then((walkData: FeatureCollection) => {
      addLayer(
        "walks",
        walkData.features.map((f) => jsonFormat.readFeature(f))
      );
      window.setTimeout(() => map.updateSize(), 100);
      loadRoute();
    })
    .catch((err) => console.error(err));
};

document.onreadystatechange = function startApplication() {
  if ("interactive" === document.readyState) {
    start();
  }
};

(function detectIE() {
  var ua = window.navigator.userAgent;
  var msie = ua.indexOf("MSIE ");
  var trident = ua.indexOf("Trident/");
  var edge = ua.indexOf("Edge/");
  const msgFR =
    "WoodWideWeb est optimisé pour des navigateurs actuels. Pour votre confort, nous vous invitons à poursuivre votre visite sur un navigateur récent.";
  const msgNL =
    "WoodWideWeb is geoptimaliseerd voor de huidige browsers. Voor uw gemak nodigen wij u uit om uw bezoek voort te zetten met een recente browser.";
  const msgEN =
    "WoodWideWeb is optimized for current browsers. For your convenience, we invite you to continue your visit on a recent browser.";
  if (msie > 0) {
    alert(msgFR + "\n \n" + msgNL + "\n \n" + msgEN);
  } else if (trident > 0) {
    // IE 11
    alert(msgFR + "\n \n" + msgNL + "\n \n" + msgEN);
  } else if (edge > 0) {
    // Edge
    //Do some stuff
  }
  // other browser
  else return false;
})();
