
const { resolve, basename, dirname } = require('path');
const webpack = require('webpack');



function configure(ROOT) {
    if (typeof ROOT !== 'string') {
        throw (new Error('ROOT argument is mandatory'));
    }

    const NAME = basename(ROOT);
    const BUNDLE_ENTRY_PATH = resolve(ROOT, 'src/index.ts');
    const OUTPUT_DIR = resolve(ROOT, '../wood/wood_core/static/wood_core/js/');

    console.log(`ROOT ${ROOT}`);
    console.log(`BUNDLE_ENTRY_PATH ${BUNDLE_ENTRY_PATH}`);
    console.log(`OUTPUT_DIR ${OUTPUT_DIR}`);




    const config = {
        mode: 'development',
        context: resolve(ROOT),
        entry: {
            bundle: BUNDLE_ENTRY_PATH,
        },

        output: {
            path: OUTPUT_DIR,
            publicPath: '/',
            filename: '[name].js',
        },

        resolve: {
            // proj4 module declaration is not consistent with its ditribution
            mainFields: ["browser", "main", /* "module" */],
            extensions: ['.ts', '.js'],
        },

        module: {
            rules: [
                {
                    enforce: 'pre',
                    test: /\.js$/,
                    exclude: resolve(ROOT, '../node_modules/'),
                    loader: 'source-map-loader',
                },
                {
                    enforce: 'pre',
                    test: /\.ts$/,
                    use: "source-map-loader"
                },
                {
                    test: /\.ts$/,
                    use: [
                        { loader: 'babel-loader' },
                        { loader: 'ts-loader' },
                    ],
                }
            ]
        },
        devtool: 'source-map',
    };

    return config;
}


const compiler = webpack(configure(dirname(__filename)));

const watching = compiler.watch({}, (err, stats) => {
    if (err) {
        console.error(`Failed to produce a bundle (err) => ${err}`)
    }
    else if (stats.hasErrors()) {
        console.error(`Failed to produce a bundle (hasErrors) => ${stats.toJson().errors}`)
    }
    else {
        console.log(`Success Build @ ${Date()}`)
    }
})
