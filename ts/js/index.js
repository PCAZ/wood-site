"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const openlayers_1 = require("openlayers");
const proj4 = require("proj4");
openlayers_1.proj.setProj4(proj4);
proj4.defs('EPSG:31370', '+proj=lcc +lat_1=51.16666723333333 +lat_2=49.8333339 +lat_0=90 +lon_0=4.367486666666666 +x_0=150000.013 +y_0=5400088.438 +ellps=intl +towgs84=-106.8686,52.2978,-103.7239,-0.3366,0.457,-1.8422,-1.2747 +units=m +no_defs');
const lang = window.woodLang;
const records = {
    fullNameField: {
        fr: 'nom_fr',
        nl: 'nom_nl',
        en: 'nom_en',
    },
    latinNameField: {
        fr: 'latin',
        nl: 'latin',
        en: 'latin',
    },
    familyNameField: {
        fr: 'famille_fr',
        nl: 'famille_nl',
        en: 'famille_en',
    },
    addressField: {
        fr: 'adresse_1',
        nl: 'adres_1',
        en: '__does_not_exist__',
    },
    fileName: {
        fr: 'wood_file',
        nl: 'wood_file',
        en: 'wood_file',
    },
    toPage: {
        fr: 'Voir la fiche',
        nl: 'Zie pagina',
        en: 'Go to page',
    }
};
const getRecord = (k) => records[k][lang];
const getFromProps = (k) => (props, defaultValue) => {
    const tk = records[k][lang];
    if (props && tk in props) {
        return props[tk];
    }
    return defaultValue;
};
const getName = getFromProps('fullNameField');
const getFamily = getFromProps('familyNameField');
const getFileName = getFromProps('fileName');
const EPSG31370 = new openlayers_1.proj.Projection({
    code: 'EPSG:31370',
    extent: [14697.30, 22635.80, 291071.84, 246456.18],
    worldExtent: [2.5, 49.5, 6.4, 51.51],
    global: false,
    getPointResolution: (r) => r,
});
openlayers_1.proj.addProjection(EPSG31370);
const wmsOrthoConfig = {
    srs: 'EPSG:31370',
    params: {
        LAYERS: 'Urbis:Ortho2017',
        VERSION: '1.1.1',
    },
    url: 'https://geoservices-urbis.irisnet.be/geoserver/ows'
};
const jsonFormat = new openlayers_1.format.GeoJSON();
const isRenderFeature = (a) => {
    return 'getExtent' in a;
};
const bufferFeature = (f) => (radius) => {
    if (isRenderFeature(f)) {
        return (new openlayers_1.geom.Circle(openlayers_1.extent.getCenter(f.getExtent()), radius));
    }
    return (new openlayers_1.geom.Circle(f.getGeometry().getCoordinates(), radius));
};
const lineFeature = (f) => {
    const coords = isRenderFeature(f) ?
        openlayers_1.extent.getCenter(f.getExtent()) :
        f.getGeometry().getCoordinates();
    return (new openlayers_1.geom.MultiLineString([
        [[coords[0], 0], [coords[0], coords[1] - 5]],
        [[0, coords[1]], [coords[0], coords[1] - 5]],
    ]));
};
const strokeStyle = (width, color) => new openlayers_1.style.Stroke({
    width,
    color,
});
const baseStyleWidth = 9;
const baseStyleRadius = 12;
const highlightWidth = 9;
const highlightRadius = 14;
const baseStyle = (color) => (f) => new openlayers_1.style.Style({
    stroke: strokeStyle(baseStyleWidth, color),
    geometry: bufferFeature(f)(baseStyleRadius),
});
const focusStyle = (f) => [
    new openlayers_1.style.Style({
        stroke: strokeStyle(1, 'white'),
        geometry: lineFeature(f),
    }),
    new openlayers_1.style.Style({
        stroke: strokeStyle(baseStyleWidth, '#d6ca55'),
        geometry: bufferFeature(f)(baseStyleRadius),
    })
];
const inventoryStyle = (f) => new openlayers_1.style.Style({
    stroke: strokeStyle(2, "#97cfb1"),
    geometry: bufferFeature(f)(1),
});
const avecPortraitStyle = (f) => [
    new openlayers_1.style.Style({
        stroke: strokeStyle(baseStyleWidth, '#97cfb1'),
        geometry: bufferFeature(f)(baseStyleRadius),
    }),
    new openlayers_1.style.Style({
        stroke: strokeStyle(baseStyleWidth / 2, 'white'),
        geometry: bufferFeature(f)(baseStyleRadius - 2),
    })
];
const Styles = {
    'rmqb': baseStyle('#97cfb1'),
    'rmqe': baseStyle('#ead341'),
    'plnt': baseStyle('#cd9351'),
    'adopte': baseStyle('#32d148'),
    'remarquableAvecPortrait': avecPortraitStyle,
    'focus': focusStyle,
    'inventory': inventoryStyle,
};
const getCategory = (f) => {
    const props = f.getProperties();
    const h = props['highlight'];
    const cat = props['category'];
    if (h) {
        return 'focus';
    }
    if ('rmqb' === cat && 'has_portrait' in props && props['has_portrait']) {
        return 'remarquableAvecPortrait';
    }
    return cat;
};
const makeDefaultStyle = () => (f) => {
    const cat = getCategory(f);
    const s = Styles[cat];
    return s(f);
};
const makeHighlightStyle = () => (f) => {
    const cat = getCategory(f);
    const catStyle = Styles[cat](f);
    const selectStyle = cat === 'inventory' ?
        ([
            new openlayers_1.style.Style({
                stroke: strokeStyle(highlightWidth / 2, 'white'),
                geometry: bufferFeature(f)(highlightRadius / 2),
            }),
        ]) :
        ([
            new openlayers_1.style.Style({
                stroke: strokeStyle(highlightWidth, 'white'),
                geometry: bufferFeature(f)(highlightRadius),
            })
        ]);
    if (Array.isArray(catStyle)) {
        return catStyle.concat(selectStyle);
    }
    return [catStyle].concat(selectStyle);
};
const makeSelectStyle = () => (f) => {
    const catStyle = Styles[getCategory(f)](f);
    const selectStyle = [
        new openlayers_1.style.Style({
            stroke: strokeStyle(highlightWidth, 'white'),
            geometry: bufferFeature(f)(highlightRadius),
        }),
        new openlayers_1.style.Style({
            stroke: strokeStyle(highlightWidth / 2, 'grey'),
            geometry: bufferFeature(f)(highlightRadius + highlightWidth),
        }),
    ];
    if (Array.isArray(catStyle)) {
        return catStyle.concat(selectStyle);
    }
    return [catStyle].concat(selectStyle);
};
const boxedText = (t, className) => {
    const e = document.createElement('div');
    e.setAttribute('class', className);
    e.appendChild(document.createTextNode(t));
    return e;
};
const boxedAnchor = (t, href, className) => {
    const e = document.createElement('div');
    const a = document.createElement('a');
    e.setAttribute('class', className);
    a.setAttribute('href', href);
    a.appendChild(document.createTextNode(t));
    e.appendChild(a);
    return e;
};
const makeImage = (id, cat) => {
    const i = document.createElement('img');
    if (cat === 'adopte') {
        i.setAttribute('src', `/media/contrib/${id}/600`);
    }
    else {
        i.setAttribute('src', `/media/tree/${id}/600`);
    }
    return i;
};
const makePopup = (f, props) => {
    const element = document.createElement('div');
    const cat = getCategory(f);
    element.setAttribute('class', 'popup');
    element.id = props.id;
    element.appendChild(makeImage(element.id, cat));
    element.appendChild(boxedText(getName(props, ''), 'name'));
    if (cat === 'focus') {
        const focusList = document.createElement('div');
        focusList.setAttribute('class', 'focus-list');
        element.appendChild(focusList);
        fetch(`/${lang}/focus/${props.id}`)
            .then(response => response.json())
            .then((data) => {
            data.focus.forEach((f) => {
                const fe = document.createElement('div');
                fe.setAttribute('class', 'focus-item');
                fe.appendChild(document.createTextNode(f.note));
                focusList.appendChild(fe);
            });
        });
    }
    if (cat === 'adopte') {
        const adopte = document.createElement('div');
        adopte.setAttribute('class', 'adopte');
        element.appendChild(adopte);
        fetch(`/adopte/${props.id}`)
            .then(response => response.json())
            .then((data) => {
            adopte.appendChild(document.createTextNode(data.description));
        });
    }
    element.appendChild(boxedAnchor(getRecord('toPage'), getFileName(props, ''), 'link'));
    return element;
};
const makeLabel = (_f, props) => {
    const element = document.createElement('div');
    element.setAttribute('class', 'label');
    element.id = props.id;
    element.appendChild(boxedText(getName(props, ''), 'name'));
    return element;
};
const removeElement = (selector) => () => {
    const preview = document.querySelector(selector);
    if (preview) {
        let c = preview.firstChild;
        while (c) {
            preview.removeChild(c);
            c = preview.firstChild;
        }
    }
};
const addElement = (selector) => (e) => {
    const preview = document.querySelector(selector);
    if (preview) {
        preview.appendChild(e);
    }
};
const removeOverlay = removeElement('.tree-preview');
const addOverlay = addElement('.tree-preview');
const removeLabel = removeElement('.tree-label');
const addLabel = addElement('.tree-label');
const makeMap = () => {
    const view = new openlayers_1.View({
        projection: EPSG31370,
        center: [148651, 170897],
        rotation: 0,
        zoom: 6,
    });
    const map = new openlayers_1.Map({
        target: 'map',
        view,
    });
    const layers = [];
    const baseLayer = new openlayers_1.layer.Tile({
        source: new openlayers_1.source.TileWMS({
            projection: openlayers_1.proj.get(wmsOrthoConfig.srs),
            params: Object.assign({}, wmsOrthoConfig.params, { 'TILED': true }),
            url: wmsOrthoConfig.url,
        }),
    });
    map.addLayer(baseLayer);
    let popup = null;
    let label = null;
    let lastMove = performance.now();
    map.on('pointermove', (e) => {
        const t = performance.now();
        if (t - lastMove < 100) {
            return;
        }
        lastMove = t;
        const pixel = e.pixel !== undefined ? e.pixel : [0, 0];
        const fs = map.getFeaturesAtPixel(pixel);
        if (fs && fs.length > 0) {
            const props = fs[0].getProperties();
            if (props) {
                if (label && label.id !== props.id) {
                    removeLabel();
                    label = null;
                }
                if (label === null) {
                    label = makeLabel(fs[0], props);
                    addLabel(label);
                }
            }
        }
        else if (label !== null) {
            removeLabel();
            label = null;
        }
    });
    const select = new openlayers_1.interaction.Select({
        multi: true,
        style: makeSelectStyle(),
    });
    select.on('select', (e) => {
        const fs = e.target.getFeatures();
        if (fs.getLength() === 0) {
            removeOverlay();
            popup = null;
            return;
        }
        const selectOnlyOneRegular = (_s) => 'OnlyOneRegular';
        const selectOnlyOneInventory = (_s) => 'OnlyOneInventory';
        const selectOnlyManyRegular = (_s) => 'OnlyManyRegular';
        const selectOnlyManyInventory = (_s) => 'OnlyManyInventory';
        const selectMixedOneRegular = (_s) => 'MixedOneRegular';
        const selectMixedManyRegular = (_s) => 'MixedManyRegular';
        const selState = fs.getArray().reduce((acc, f) => {
            const props = f.getProperties();
            const isInventory = props.category && props.category === 'inventory';
            if (isInventory) {
                switch (acc) {
                    case 'Nothing': return selectOnlyOneInventory(acc);
                    case 'OnlyOneRegular': return selectMixedOneRegular(acc);
                    case 'OnlyManyRegular': return selectMixedManyRegular(acc);
                    case 'OnlyOneInventory': return selectOnlyManyInventory(acc);
                    case 'OnlyManyInventory': return selectOnlyManyInventory(acc);
                    case 'MixedOneRegular': return selectMixedOneRegular(acc);
                    case 'MixedManyRegular': return selectMixedManyRegular(acc);
                }
            }
            switch (acc) {
                case 'Nothing': return selectOnlyOneRegular(acc);
                case 'OnlyOneRegular': return selectOnlyManyRegular(acc);
                case 'OnlyManyRegular': return selectOnlyManyRegular(acc);
                case 'OnlyOneInventory': return selectMixedOneRegular(acc);
                case 'OnlyManyInventory': return selectMixedOneRegular(acc);
                case 'MixedOneRegular': return selectMixedManyRegular(acc);
                case 'MixedManyRegular': return selectMixedManyRegular(acc);
            }
        }, 'Nothing');
        console.log(selState);
        const rfs = fs.getArray().filter(f => {
            const props = f.getProperties();
            const isInventory = props.category && props.category === 'inventory';
            return !isInventory;
        });
        const ifs = fs.getArray().filter(f => {
            const props = f.getProperties();
            const isInventory = props.category && props.category === 'inventory';
            return isInventory;
        });
        const withFirst = (f) => {
            rfs.forEach((ft, idx) => {
                if (0 === idx) {
                    f(ft);
                }
                else {
                    fs.remove(ft);
                }
            });
        };
        const clearSelectInventory = (_s) => ifs.forEach(f => fs.remove(f));
        const clearSelectAll = (_s) => {
            fs.clear();
            removeOverlay();
            popup = null;
        };
        const showFirstRegular = (_s) => withFirst((f) => {
            const props = f.getProperties();
            if (props) {
                if (popup && popup.id !== props.id) {
                    removeOverlay();
                    popup = null;
                }
                if (popup === null) {
                    popup = makePopup(f, props);
                    addOverlay(popup);
                }
            }
        });
        switch (selState) {
            case 'Nothing': return;
            case 'OnlyOneRegular': return showFirstRegular(selState);
            case 'OnlyManyRegular': return showFirstRegular(selState);
            case 'OnlyOneInventory': return clearSelectAll(selState);
            case 'OnlyManyInventory': return clearSelectAll(selState);
            case 'MixedOneRegular':
                showFirstRegular(selState);
                return clearSelectInventory(selState);
            case 'MixedManyRegular':
                showFirstRegular(selState);
                return clearSelectInventory(selState);
        }
    });
    map.addInteraction(select);
    const addLayer = (name, features) => {
        const woodSource = new openlayers_1.source.Vector();
        const woodLayer = new openlayers_1.layer.Vector({
            source: woodSource,
            style: makeDefaultStyle(),
        });
        woodLayer.set('name', name);
        layers.push(woodLayer);
        woodSource.addFeatures(features);
        map.addLayer(woodLayer);
    };
    const visible = (layerNames) => layers.forEach(l => l.setVisible(layerNames.indexOf(l.get('name')) >= 0));
    const highlight = (layerNames) => {
        layers.forEach(l => l.setStyle(makeDefaultStyle()));
        layers.forEach((l) => {
            const name = l.get('name');
            if (layerNames.indexOf(name) >= 0) {
                console.log(`highlight ${name} ${layerNames}`);
                l.setStyle(makeHighlightStyle());
            }
        });
    };
    return {
        addLayer,
        visible,
        highlight,
        map,
    };
};
class Toggler {
    constructor(name) {
        this.name = name;
        this.value = false;
        this.listeners = [];
    }
    toggle() {
        this.value = !this.value;
        this.listeners.forEach(f => f(this.value));
        return this.value;
    }
    concat(xs) {
        if (this.value) {
            return xs.concat([this.name]);
        }
        return xs;
    }
    onToggle(cb) {
        this.listeners.push(cb);
    }
}
const makeFilterItem = (name) => {
    const node = document.createElement('div');
    node.setAttribute('class', 'legend-item');
    const toggle = new Toggler(name);
    node.innerText = name;
    node.addEventListener('click', () => {
        const togState = toggle.toggle();
        if (togState) {
            node.setAttribute('class', 'legend-item highlight');
        }
        else {
            node.setAttribute('class', 'legend-item');
        }
    }, false);
    return { node, toggle };
};
const start = () => {
    const { addLayer, highlight } = makeMap();
    const speciesSet = new Set();
    const loaders = document.querySelectorAll('.data-loader');
    loaders.forEach(e => e.classList.replace('none', 'loading'));
    fetch('/atlas/data/')
        .then(response => response.json())
        .then((geoData) => {
        geoData.features
            .map(f => getFamily(f.properties, ''))
            .filter(name => name !== '')
            .forEach(name => speciesSet.add(name));
        const species = Array.from(speciesSet.values()).sort();
        species.forEach(name => addLayer(name, geoData.features
            .filter(f => getFamily(f.properties, '') === name)
            .map(f => jsonFormat.readFeature(f))));
        const filterElem = document.getElementById('specie-filter');
        const toggles = [];
        const toggleHandler = () => highlight(toggles.reduce((acc, t) => t.concat(acc), []));
        if (filterElem) {
            species.forEach((name) => {
                const { node, toggle } = makeFilterItem(name);
                toggle.onToggle(toggleHandler);
                toggles.push(toggle);
                filterElem.appendChild(node);
            });
        }
        loaders.forEach(e => e.classList.replace('loading', 'loaded'));
    });
};
document.onreadystatechange = function startApplication() {
    if ('interactive' === document.readyState) {
        start();
    }
};
//# sourceMappingURL=index.js.map