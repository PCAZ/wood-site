from django.apps import AppConfig


class WoodLinguaConfig(AppConfig):
    name = 'wood_lingua'
