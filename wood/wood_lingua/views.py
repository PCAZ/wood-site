from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import (
    JsonResponse, 
    HttpResponseServerError, 
    HttpResponseForbidden,
    FileResponse,
    Http404,
    )


from wood_lingua.models.message import get_model

LANGS =  ('fr', 'nl', 'en')

def _post_add_record(request, message_type):
    model = get_model(message_type)
    data = dict()
    for lang in LANGS:
        data[lang] = request.POST.get(lang, '')
    instance = model.objects.create(**data)

    return JsonResponse(dict(id=instance.id, data=instance.to_dict()))

def _post_update_record(request, message_type, id):
    model = get_model(message_type)
    instance = get_object_or_404(model, pk=id)
    
    for lang in LANGS:
        setattr(instance, lang, request.POST.get(lang, ''))

    instance.save()

    return JsonResponse(dict(id=instance.id, data=instance.to_dict()))

@login_required
def create_record(request, message_type):
    if 'POST' == request.method:
        return _post_add_record(request, message_type)
    return HttpResponseForbidden()


@login_required
def update_record(request, message_type, id):
    if 'POST' == request.method or 'PUT' == request.method:
        return _post_update_record(request, message_type, id)
    return HttpResponseForbidden()
    