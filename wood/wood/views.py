from django.contrib.auth.views import LoginView, LogoutView
from django_registration.backends.one_step.views import RegistrationView



class WoodLogin(LoginView):

    # def get_template_names(self):
    #     lang = self.kwargs.get('lang', 'en')
    #     return ['registration/login_{}.html'.format(lang)]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(self.kwargs)
        return context

class WoodRegister(RegistrationView):

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(self.kwargs)
        return context

class WoodLogout(LogoutView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(self.kwargs)
        return context