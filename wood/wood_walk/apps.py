from django.apps import AppConfig


class WoodWalkConfig(AppConfig):
    name = 'wood_walk'
