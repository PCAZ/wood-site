from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse

from wood_walk.models import Walk


def list_view(request, lang):
    u = request.user
    if u.is_authenticated and u.is_superuser:
        walks = Walk.objects.all()
    else:
        walks = Walk.objects.filter(validated=True)
    context = dict(
        lang=lang,
        walks=walks,
    )
    return render(request, 'wood_walk/list.html', context)


def single_view(request, lang, wid):
    walk = get_object_or_404(Walk, id=wid)
    context = dict(
        lang=lang,
        walk=walk,
    )
    return render(request, 'wood_walk/single.html', context)

def path_view(request):
    u = request.user
    if u.is_authenticated and u.is_superuser:
        walks = Walk.objects.all()
    else:
        walks = Walk.objects.filter(validated=True)
    fc = dict(type='FeatureCollection')
    features = []
    for w in walks:
        features.append(w.as_feature())

    fc['features'] = features
    return JsonResponse(fc)

