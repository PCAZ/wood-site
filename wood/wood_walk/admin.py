from django.contrib import admin

from wood_walk.models import Walk, WalkEdge
from wood_core.admin import GisModelAdmin

class WalkEdgeAdmin(GisModelAdmin):
    list_display = ('walk', 'in_node', 'out_node')
    map_template = 'wood_walk/admin/edge_gis.html'
    list_filter = (
        'walk',
    )
    autocomplete_fields = ['in_node', 'out_node']
    

admin.site.register(Walk)
admin.site.register(WalkEdge, WalkEdgeAdmin)