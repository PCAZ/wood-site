from django.contrib.gis.db import models

from wood_core.models.subject import Tree
from wood_lingua.models import message_field

from easy_thumbnails.fields import ThumbnailerImageField
from pyproj import CRS, Transformer


wgs = CRS('EPSG:4326')
lambert72 = CRS('EPSG:31370')
transformer = Transformer.from_crs(wgs, lambert72)

def tc_line(coords):
    return [transformer.transform(c[1], c[0]) for c in coords]


class Walk(models.Model):
    id = models.AutoField(primary_key=True)
    title = message_field('walk_title')
    subtitle = message_field('walk_subtitle')
    summary = message_field('walk_summary')
    validated = models.BooleanField(default=False)
    
    def edges(self):
        return order_edges(list(self.walkedge_set.all()))
    
    def as_feature(self):
        # path_coord contiendra des "path" qui sont des linestring
        path_coordinates = [] 
        for e in self.edges():
            path_coordinates.append(tc_line(e.path.coords))
        
        feature = {
            "type":"Feature",
            "properties":{
                "id":self.id,
                "name":self.title.to_dict(),
                "category": "path",
            },
            "geometry":{
                "type": "MultiLineString",
                "coordinates": path_coordinates,
            }
        }
        return feature
        

    def __str__(self):
        return str(self.title)


class WalkEdge(models.Model):
    id = models.AutoField(primary_key=True)
    in_node = models.ForeignKey(
        Tree, on_delete=models.CASCADE, related_name='walk_in_node')
    out_node = models.ForeignKey(
        Tree, on_delete=models.CASCADE, related_name='walk_out_node')
    info = message_field('walk_stop_info')

    walk = models.ForeignKey(Walk, on_delete=models.CASCADE)
    image = ThumbnailerImageField(
        upload_to='walk_edge/', null=True, blank=True)

    path = models.LineStringField('path',srid=4326)

    def __str__(self):
        return '{walk} ({nin} --> {nout})'.format(
            walk=self.walk,
            nin=self.in_node,
            nout=self.out_node,
        )


class WalkRootError(Exception):
    pass


def find_in(edges, tid):
    for e in edges:
        if tid == e.in_node.id:
            return e
    return None


def find_out(edges, tid):
    for e in edges:
        if tid == e.out_node.id:
            return e
    return None


def find_root(edges):
    for e in edges:
        if find_out(edges, e.in_node.id) is None:
            return e

    raise WalkRootError()


def order_edges(edges):
    root = find_root(edges)
    current_out = root.out_node.id
    yield root
    while True:
        e = find_in(edges, current_out)
        if e is None:
            # raise StopIteration()
            return
        current_out = e.out_node.id
        yield e