from django.urls import path, include

from .views import (
    single_view,
    list_view,
    path_view,
)

urlpatterns = [
    path('<lang>/walk/', list_view, name='wood-walk-list'),
    path('walk/paths/', path_view, name='wood-walk-paths'),
    path('<lang>/walk/<wid>.html', single_view, name='wood-walk-single'),
]
