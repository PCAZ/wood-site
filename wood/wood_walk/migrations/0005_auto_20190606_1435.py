# Generated by Django 2.1.1 on 2019-06-06 14:35

import django.contrib.gis.db.models.fields
from django.db import migrations

# We commented the code below because Spatialate doesn't support AlterFields

class Migration(migrations.Migration):

    dependencies = [
        ('wood_walk', '0004_auto_20190606_1339'),
    ]

    operations = [
        # migrations.AlterField(
        #     model_name='walkedge',
        #     name='path',
        #     field=django.contrib.gis.db.models.fields.LineStringField(srid=4326, verbose_name='path'),
        # ),
    ]
