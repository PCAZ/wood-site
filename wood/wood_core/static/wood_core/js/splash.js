(function () {
    function removeElem(e) {
        e.parentElement.removeChild(e)
    }

    function init() {
        let videoPlay = document.getElementById("video-play");
        let videoPause = document.getElementById("video-pause");

        videoPlay.addEventListener('click', (evt) => {
            // let myVideo = document.getElementById("video1");
            // myVideo.play();
            // removeElem(videoPlay)
            videoPlay.setAttribute('class', 'splash hidden')
            videoPause.setAttribute('class', 'splash--close')
        })
        videoPause.addEventListener('click', () => {
            // let myVideo = document.getElementById("video1");
            // let splash = document.getElementById("Splash");
            // removeElem(splash)
            videoPlay.setAttribute('class', 'splash--play')
            videoPause.setAttribute('class', 'splash hidden')
        })
    }

    document.onreadystatechange = function startApplication() {
        if ('interactive' === document.readyState) {
            init()
        }
    };
})()