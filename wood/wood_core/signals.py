from multiprocessing import Process, Pipe
from collections import namedtuple
import logging

from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.mail import send_mail, mail_managers
from django.conf import settings
from django.template.loader import get_template
from django.core.cache import cache

from .models.contrib import ContribLog, TreeLog

logger = logging.getLogger(__name__)

EMAIL_FROM = getattr(settings, "EMAIL_FROM", "default@example.com")

TREE_CONTRIB_MAIL_TEMPLATE = get_template('wood_core/contrib/mail_created.txt')
TREE_CONTRIB_MAIL_ADMIN = get_template(
    'wood_core/contrib/mail_created_admin.txt')

TREE_VALID_MAIL_TEMPLATE = get_template('wood_core/contrib/mail_validated.txt')

TREE_MAIL_SUBJECT = 'Your tree on Wood Wide Web'
TREE_MAIL_SUBJECT_ADMIN = 'New tree on Wood Wide Web'

ICONTRIB_MAIL_TEMPLATE = {
    'nl': get_template('wood_core/contrib/mail_icontrib.txt'),
    'en': get_template('wood_core/contrib/mail_icontrib.txt'),
    'fr': get_template('wood_core/contrib/mail_icontrib.txt'),
}

TCONTRIB_MAIL_TEMPLATE = {
    'nl': get_template('wood_core/contrib/mail_tcontrib.txt'),
    'en': get_template('wood_core/contrib/mail_tcontrib.txt'),
    'fr': get_template('wood_core/contrib/mail_tcontrib.txt'),
}

CONTRIB_MAIL_SUBJECT_ADMIN = 'New contribution'

MailUserCommand = namedtuple(
    'MailUserCommand',
    [
        'level',  # 'user'
        'subject',
        'body',
        'user_email'
    ])

MailManagerCommand = namedtuple(
    'MailManagerCommand',
    [
        'level',  # 'manager'
        'subject',
        'body',
    ])



def process_mail(com):
    if com.level == 'user':
        send_mail(
            com.subject,
            com.body,
            EMAIL_FROM,
            [com.user_email],
            fail_silently=False,
        )
    elif com.level == 'manager':
        mail_managers(
            com.subject,
            com.body,
        )


def mailer(conn):
    logger.info('Started Mailer')
    while True:
        try:
            msg = conn.recv()
            if isinstance(msg, (MailUserCommand, MailManagerCommand,)):
                process_mail(msg)
            else:
                logger.error('MailerError: unkowkn message type {}'.format(type(msg)))
        except EOFError:
            logger.info('Stoped Mailer: end of pipe')
            return
        except Exception as ex:
            logger.error('MailerError: {}'.format(ex))


class MailBox:
    def __init__(self):
        rx, tx = Pipe(duplex=False)
        self._tx = tx
        self._process = Process(target=mailer, args=(rx, ))
        self._process.daemon = True
        self._process.start()

    def send_user(self, subject, body, user_email):
        self._tx.send(MailUserCommand(
            'user',
            subject,
            body,
            user_email,
        ))

    def send_managers(self, subject, body):
        self._tx.send(MailManagerCommand(
            'manager',
            subject,
            body,
        ))


mail_box = MailBox()


def on_tree_contrib_save(sender, instance, created, **kwargs):
    '''
        Send mail to contributors when their tree is created or validated
    '''
    if created:
        TreeLog.objects.create(tree=instance, event=TreeLog.CREATED)
        ctx = dict(user=instance.contributor, contrib=instance)
        
        #temporary bad fixing of bug Error 500 when admin change or create a tree
        email = 'nina.w@atelier-cartographique.be'
        if instance.contributor != None:
            email = instance.contributor.email

        mail_box.send_user(
            TREE_MAIL_SUBJECT,
            TREE_CONTRIB_MAIL_TEMPLATE.render(ctx),
            email,
        )
        mail_box.send_managers(
            TREE_MAIL_SUBJECT_ADMIN,
            TREE_CONTRIB_MAIL_ADMIN.render(ctx),
        )

    elif instance.validated:
        email = 'nina.w@atelier-cartographique.be'
        if instance.contributor != None:
            email = instance.contributor.email
            
        ctx = dict(user=instance.contributor, contrib=instance)
        has_validated = ContribLog.find_logs_for(
            instance.id).filter(event=ContribLog.VALIDATED).count() > 0
        if not has_validated:
            TreeLog.objects.create(tree=instance, event=TreeLog.VALIDATED)
            ctx = dict(user=instance.contributor, contrib=instance)

            mail_box.send_user(TREE_MAIL_SUBJECT,
                               TREE_VALID_MAIL_TEMPLATE.render(ctx),
                               email)


def on_tree_save(sender, instance, created, **kwargs):
    '''
        Clear the cache when a tree is modified or added
    '''
    cache.clear()  # simple and strong -pm


def on_img_contrib_save(sender, instance, created, **kwargs):
    '''
        Send a mail to admin when an image-contribution is made
    '''
    if created:
        ctx = dict(user=instance.contributor, contrib=instance)

        mail_box.send_managers(
            CONTRIB_MAIL_SUBJECT_ADMIN,
            ICONTRIB_MAIL_TEMPLATE[instance.lang].render(ctx))


def on_txt_contrib_save(sender, instance, created, **kwargs):
    '''
        Send a mail to admin when a text-contribution is made
    '''
    if created:
        ctx = dict(user=instance.contributor, contrib=instance)

        mail_box.send_managers(
            CONTRIB_MAIL_SUBJECT_ADMIN,
            TCONTRIB_MAIL_TEMPLATE[instance.lang].render(ctx),
        )


def connect():
    print('signals.connect')

    post_save.connect(
        on_img_contrib_save,
        sender='wood_core.ImageContribution',
        dispatch_uid='signal_save_icontribution')
    post_save.connect(
        on_txt_contrib_save,
        sender='wood_core.TextContribution',
        dispatch_uid='signal_save_tcontribution')

    post_save.connect(
        on_tree_contrib_save,
        sender='wood_core.Tree',
        dispatch_uid='signal_tree')

    post_save.connect(
        on_tree_save, sender='wood_core.Tree', dispatch_uid='signal_save_tree')
