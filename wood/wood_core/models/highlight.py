#  Copyright (C) 2018 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from django.contrib.gis.db import models
from wood_lingua.models import message_field


class Highlight(models.Model):
    id = models.AutoField(primary_key=True)
    tree = models.ForeignKey(to='wood_core.Tree', on_delete=models.CASCADE)
    start = models.DateTimeField()
    end = models.DateTimeField()
    note = message_field('highlight_note')

    def to_dict(self, lang):
        return {
            "start": self.start.isoformat(),
            "end": self.end.isoformat(),
            "note": self.note.tr(lang),
        }

    def __str__(self):
        return str(self.note)
