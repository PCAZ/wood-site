#  Copyright (C) 2018 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from urllib.request import Request
from datetime import datetime, timezone
from django.contrib.gis.db import models
from django.conf import settings
from easy_thumbnails.fields import ThumbnailerImageField

from wood_core.fields import PointField
from wood_lingua.models import message_field
from wood_botanique.models.species import Taxon

from .media import picture, slugify
from .ancillary import BaseLink
from .service import TreeService


class TreeManager(models.Manager):
    def get_queryset(self):
        select = (
            'name',
            'description',
            'street_address',
            'city',
            'post_code',
        )
        return super().get_queryset().select_related(*select)


def upload_to_photomaton(instance, fn):
    id = instance.id
    return 'phototomaton/{}/{}'.format(id, slugify(fn))


class Tree(models.Model):
    """Models a general Tree (from inventory, contribution, ...)
    """
    objects = TreeManager()

    REMARQUABLE = 'rmqb'
    REMARQUE = 'rmqe'
    ADOPTE = 'adpt'
    PLANTE = 'plnt'
    INVENTAIRE = 'inv'
    CONTRIBUE = 'ctrb'
    CATEGORY = (
        (REMARQUABLE, 'Remarquable'),
        (REMARQUE, 'Remarqué'),
        (ADOPTE, 'Adopté'),
        (PLANTE, 'Planté'),
        (INVENTAIRE, 'Inventaire'),
        (CONTRIBUE, 'Contribué'),
    )

    id = models.AutoField(primary_key=True)
    patrimoine = models.IntegerField(blank=True, null=True)
    import_id = models.CharField(max_length=12, null=True, blank=True)
    category = models.CharField(
        max_length=4, choices=CATEGORY, null=True, blank=True)
    taxon = models.ForeignKey(
        Taxon, on_delete=models.CASCADE, null=True, blank=True)

    photomaton = ThumbnailerImageField(
        upload_to=upload_to_photomaton, null=True, blank=True)

    name = message_field('tree_name', null=True, blank=True)
    description = message_field('tree_description', null=True, blank=True)

    street_address = message_field(
        'tree_street_address', null=True, blank=True)
    city = message_field('tree_city', null=True, blank=True)
    post_code = message_field('tree_post_code', null=True, blank=True)
    position = PointField(srid=4326, null=True, blank=True)

    height = message_field('tree_height', null=True, blank=True)
    circumference = message_field('tree_circumference', null=True, blank=True)
    crown_diameter = message_field(
        'tree_crown_diameter', null=True, blank=True)

    service = models.ForeignKey(
        TreeService, on_delete=models.CASCADE, null=True, blank=True)

    #item caract
    source = models.CharField(max_length=64, null=True, blank=True)
    date = models.DateTimeField(auto_now=True, null=True, blank=True)

    # from contribution
    validated = models.BooleanField(default=False)
    contributor = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        null=True, blank=True,
    )

    def get_highlights(self):
        now = datetime.now(timezone.utc)
        return self.highlight_set.filter(start__lte=now, end__gte=now)

    def is_highlighted(self):
        return self.get_highlights().count() > 0

    def __str__(self):
        if self.name:
            return '{} ({})'.format(self.name, self.id)
        elif self.taxon:
            return '{} ({})'.format(self.taxon.name, self.id)
        return 'Tree ({})'.format(self.id)


TreePicture = picture(Tree, is_sorted=True)


class Remarqueur(BaseLink):
    url = message_field('remarqueur_url')
    label = message_field('remarqueur_label')


class Activity(BaseLink):
    url = message_field('activity_url')
    label = message_field('activity_label')


class LinkedMedia(BaseLink):
    url = models.URLField(null=True, blank=True)
    url_lingua = message_field('media_url', null=True, blank= True)
    label = message_field('linked_media_label')

    def get_url(self, lang):
        if self.url_lingua is not None:
            url_new =  getattr(self.url_lingua, lang)
            try:
                Request(url_new)
            except ValueError:
                return 'missing URL in {} for linked media {}'.format(lang, self.id)
            return url_new
        return self.url

class Jumelage(BaseLink):
    BELGIQUE = 'belgique'
    EUROPE = 'europe'
    MONDE = 'monde'
    SCOPE = (
        (BELGIQUE, 'Belgique'),
        (EUROPE, 'Europe'),
        (MONDE, 'Monde'),
    )
    scope = models.CharField(max_length=12, choices=SCOPE)

    url = models.URLField(null=True, blank=True)
    url_lingua = message_field('jumelage_url', null=True, blank= True)
    label = message_field('jumelage_label')

    def get_url(self, lang):
        if self.url_lingua is not None:
            url_new =  getattr(self.url_lingua, lang)
            try:
                Request(url_new)
            except ValueError:
                return 'missing URL in {} for twin {}'.format(lang, self.id)
            return url_new
        return self.url
