from django.db import models
from wood_lingua.models import message_field


class TreeQuality(models.Model):
    """Models a quantified quality of a subject
    """
    id = models.AutoField(primary_key=True)

    value = message_field('tree_quality')
    quantity = models.IntegerField()

    def __str__(self):
        return '{} ({})'.format(self.value, self.quantity)


def quality_field(name):
    return models.ForeignKey(
        TreeQuality,
        on_delete=models.CASCADE,
        related_name=name,
    )


class ServiceManager(models.Manager):
    def get_queryset(self):
        select = (
            'beauty',
            'biodiversity',
            'oxygeny',
            'purify',
            'filtery',
            'floody',
            'carbony',
            'sweety',
            'soily',
            'healthy',
        )
        return super().get_queryset().select_related(*select)


class TreeService(models.Model):
    objects = ServiceManager()
    id = models.AutoField(primary_key=True)

    beauty = quality_field('taxon_beauty')
    biodiversity = quality_field('taxon_biodiversity')
    oxygeny = quality_field('taxon_oxygeny')
    purify = quality_field('taxon_purify')
    filtery = quality_field('taxon_filtery')
    floody = quality_field('taxon_floody')
    carbony = quality_field('taxon_carbony')
    sweety = quality_field('taxon_sweety')
    soily = quality_field('taxon_soily')
    healthy = quality_field('taxon_healthy')

    def __str__(self):
        return 'Service of {}'.format(', '.join(
            [str(t) for t in self.tree_set.all()]))
