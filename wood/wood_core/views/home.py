from django.shortcuts import render

from wood_core.models.subject import Tree, TreePicture, Remarqueur
from wood_core.models.ancillary import Portrait


def front_page(request, lang):
    """Serve the landing page
    """
    template_name = 'wood_core/site-{}/index.html'.format(lang)
    tree_count = Tree.objects.count()
    # contrib_count = Contribution.objects.filter(validated=True).count()
    context = {
        'lang': lang,
        'counter': tree_count,
        'monthly': Portrait.objects.filter(monthly=True).first()
    }
    return render(request, template_name, context)


def about_page(request, lang):
    """Serve the landing page
    """
    template_name = 'wood_core/site-{}/about.html'.format(lang)

    actors_dict = dict()

    for r in Remarqueur.objects.all():
        try:
            lid = getattr(r.label, lang)
            if lid not in actors_dict:
                actors_dict[lid] = dict(name=lid, url=r.url, trees=[])
            actors_dict[lid]['trees'].append(r.tree)
        except Exception:
            pass

    actors = sorted([s for s in actors_dict.values()],
                    key=lambda x: x['name'].lower())

    context = dict(lang=lang, actors=actors)
    return render(request, template_name, context)

def list_all_trees(request, lang):
    """List all the trees, for referencing
    """
    template_name = 'wood_core/site_map.html'
    trees = Tree.objects.all()

    context = dict(
        lang=lang,
        trees = trees,
    )

    return render(request, template_name, context)