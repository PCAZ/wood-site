from pathlib import Path

from django.shortcuts import  get_object_or_404,  redirect
from django.http import (
    FileResponse,
    Http404,
    )
from django.urls import reverse
from django.conf import settings
from pyproj import CRS, Transformer
from easy_thumbnails.files import  Thumbnailer
from django.contrib.auth.views import redirect_to_login

from wood_botanique.models.species import TaxonPicture

from wood_core.models.subject import Tree, TreePicture
from wood_core.models.contrib import  PictureContrib,  ImageContribution

from wood_walk.models import  WalkEdge

wgs = CRS('EPSG:4326')
lambert72 = CRS('EPSG:31370')
transformer = Transformer.from_crs(wgs, lambert72)

def login_required_lingua(next_name):
# next_name is the name of the next page to redirect to after login.

    def dec(fn):
        def inner(request, lang, *args, **kwargs):
            u = request.user
            if u.is_authenticated:
                return fn(request, lang, *args, **kwargs)
            
            # if there are other arguments than the langage, like the tree-id, use it in the reverse function
            reverse_args = [lang,] + list(kwargs.values())
            return  redirect_to_login(
                reverse(next_name, args=reverse_args) , 
                reverse('login', args=(lang,)),)
        
        return inner
    return dec

def get_size(w, h, target):
    if w > h:
        r = target / w
    else:
        r = target / h
    tw = w * r
    th = h * r
    return (tw, th)

def media(request, model, id, target):
    if 'treepicture' == model:
        obj = get_object_or_404(TreePicture, pk=id)
        width, height = get_size(obj.file.width, obj.file.height, target)
        thumb = obj.file.get_thumbnail({'size': [width, height]})
        return redirect(thumb.url, permanent=True)
    elif 'tree' == model:
        obj = get_object_or_404(Tree, pk=id)

        # if obj.photomaton is not None:
        photomaton = obj.photomaton
        # elif obj.category == 'ctrb':
        #     imgcontribs = ImageContribution.objects.filter(tree_id=id)
        #     photomaton = imgcontribs[0].picture            
        
        if photomaton is None:
            raise Http404('This contrib does not have any image Yet!')
        
        width, height = get_size(photomaton.width, photomaton.height, target)
        thumb = photomaton.get_thumbnail({'size': [width, height]})
        return redirect(thumb.url, permanent=True)
    elif 'taxon' == model:
        obj = get_object_or_404(TaxonPicture, pk=id)
        width, height = get_size(obj.file.width, obj.file.height, target)
        thumb = obj.file.get_thumbnail({'size': [width, height]})
        return redirect(thumb.url, permanent=True)
    elif 'contrib' == model:
        # obj = get_object_or_404(Contribution, pk=id)
        obj = get_object_or_404(Tree, pk=id)
        imgcontribs = ImageContribution.objects.filter(tree_id=id)
        contrib = imgcontribs[0].picture
        if contrib is None:
            raise Http404('This contrib does not have any image Yet!')
        thumb = contrib.image.get_thumbnail(
            {
                'size': [target, target],
            },
            generate=True,
            save=True,
        )
        thumb.seek(0)
        return FileResponse(thumb)
    elif 'img-contrib' == model:
        obj = get_object_or_404(ImageContribution, pk=id)
        thumb = obj.picture.get_thumbnail(
            {
                'size':[target, target],
            },
            generate=True,
            save=True,
        )
        thumb.seek(0)
        return FileResponse(thumb)
    
    elif 'contrib-extra' == model:
        obj = get_object_or_404(PictureContrib, pk=id)
        thumb = obj.file.get_thumbnail(
            {
                'size': [target, target],
            },
            generate=True,
            save=True,
        )
        thumb.seek(0)
        return FileResponse(thumb)
    elif 'walkedge' == model:
        obj = get_object_or_404(WalkEdge, pk = id)
        thumb = obj.image.get_thumbnail(
            {
                'size': [target, target],
            },
            generate=True,
            save=True,
        )
        thumb.seek(0)
        return FileResponse(thumb)


def media_site(request, name, target):
    static_root = Path(getattr(settings, 'STATIC_ROOT'))
    img_path = static_root.joinpath('wood_core/images', name)
    with open(img_path.as_posix(), 'rb') as f:
        thumbnailer = Thumbnailer(f, img_path.stem)
        thumb = thumbnailer.get_thumbnail({'size': [target, target]})
        return redirect(thumb.url, permanent=True)