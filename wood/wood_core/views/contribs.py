from pathlib import Path
from collections import namedtuple
from urllib.parse import urlencode

from django.shortcuts import render, get_object_or_404, get_list_or_404, redirect
from django.http import (
    JsonResponse,
    HttpResponseServerError,
    HttpResponseForbidden,
    FileResponse,
    Http404,
    HttpResponse,
    HttpResponseNotAllowed,
)
from django.contrib.auth.decorators import login_required
# from django.core.serializers import serialize
from shapely.geometry import Point
from django.urls import reverse
from django.conf import settings
from pyproj import Proj, transform
from easy_thumbnails.files import get_thumbnailer, Thumbnailer
from django.contrib.auth.views import redirect_to_login

from wood_botanique.models.species import TaxonPicture

from wood_core.models.subject import Tree, TreePicture, Remarqueur
from wood_core.models.ancillary import Portrait
from wood_core.models.contrib import Contribution, PictureContrib, TextContribution, ImageContribution

from wood_core.forms import (
    text_contrib_form,
    img_contrib_form,
    add_tree_form,
    ImgContribFormSet,
)
from wood_walk.models import Walk, WalkEdge

from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

from .common import *


def _post_add_tree(request, lang, context, obj):
    form = add_tree_form(lang, request.POST, request.FILES, instance=obj)

    if form.is_valid():
        tree = form.save(commit=False)
        tree.position = form.data['position']
        tree.category = 'adpt'

        try:
            tree.save()
            return redirect('wood-contrib', lang)
        except Exception as ex:
            return HttpResponseServerError(ex)

    context.update(dict(form=form))

    return render(request, 'wood_core/contrib/add_tree.html', context)


@login_required_lingua(next_name='wood-add-tree')
def add_tree(request, lang):
    context = dict(user=request.user, lang=lang)

    if 'POST' == request.method:
        return _post_add_tree(request, lang, context, None)
    else:
        context.update(
            dict(
                form=add_tree_form(lang)
            )
        )
    return render(request, 'wood_core/contrib/add_tree.html', context)


@login_required_lingua(next_name='wood-edit-tree')
def edit_tree(request, lang, id):
    tree = get_object_or_404(Tree, pk=id)
    context = dict(user=request.user, lang=lang)

    if tree.contributor != request.user:
        return HttpResponseForbidden('user mismatch')

    if 'POST' == request.method:
        return _post_add_tree(request, lang, context, tree)
    else:
        context.update(
            dict(
                form=add_tree_form(lang, instance=tree)
            )
        )
    return render(request, 'wood_core/contrib/add_tree.html', context)


def wms_bbox(x, y, width):
    half_width = width/2
    min_x = x - half_width
    max_x = x + half_width
    min_y = y - half_width
    max_y = y + half_width
    return (min_x, min_y, max_x, max_y)


def wms_query(bbox):

    template = {
        'SERVICE': 'WMS',
        'VERSION': '1.1.1',
        'REQUEST': 'GetMap',
        'FORMAT': 'image/png',
        'TRANSPARENT': 'true',
        'LAYERS': 'Urbis:Ortho2017',
        'TILED': 'true',
        'WIDTH': '1000',
        'HEIGHT': '1000',
        'SRS': 'EPSG:31370',
        'STYLES': '',
        'BBOX': '{},{},{},{}'.format(*bbox),
    }
    return 'https://geoservices-urbis.irisnet.be/geoserver/ows?'+urlencode(template)


@login_required_lingua(next_name='wood-contrib')
def contribution_list(request, lang):
    trees_from_contributor = Tree.objects.filter(contributor=request.user)
    other_trees = Tree.objects.exclude(contributor=request.user)
    text_contribs = TextContribution.objects.filter(contributor=request.user)
    pict_contribs = ImageContribution.objects.filter(contributor=request.user)

    # !!! tri des arbres qui ont une contribution de ce contributeur
    trees_with_contribs = []
    for t in other_trees:
        contribs = t.contributions.all()
        contributors = map(lambda c: c.contributor, contribs)
        if request.user in contributors:
            trees_with_contribs.append(t)
    trees = list(trees_from_contributor) + trees_with_contribs

    context = dict(
        lang=lang,
        trees=trees,
        # contrib_trees=trees_from_contributor,
        # other_trees=other_trees,
        text_contributions=text_contribs,
        image_contributions=pict_contribs,
        user=request.user,
    )
    return render(request, 'wood_core/contrib/list.html', context)

# Image contributions


def _post_img_contrib(request, lang, tid, context, obj):
    form = img_contrib_form(lang, request.POST, request.FILES, instance=obj)
    formset = ImgContribFormSet(
        data=request.POST, files=request.FILES)
    print("ici")
    tree = get_object_or_404(Tree, id=tid)

    if form.is_valid():
        contrib = form.save(commit=False)
        if tree.category == 'inv':
            tree.category = 'adpt'

        contrib.position = tree.position
        contrib.contributor = request.user
        # contrib.taxon = taxon
        contrib.lang = lang

        for f in formset:
            if f.is_valid():
                c = f.save(commit=False)
                c.position = tree.position
                c.contributor = request.user
                c.lang = lang
                try:
                    c.save()
                except Exception as ex:
                    return HttpResponseServerError(ex)

        try:
            contrib.save()
            tree.save()
            return redirect('wood-atlas-tree', lang, tid)
        except Exception as ex:
            return HttpResponseServerError(ex)

    return render(request, 'wood_core/contrib/img_contrib.html', context)


@login_required_lingua(next_name='wood-img-contrib-new')
def img_contribution_new(request, lang, tid):
    context = dict(user=request.user, lang=lang, tid=tid)

    if 'POST' == request.method:
        return _post_img_contrib(request, lang, tid, context, None)
    else:
        tree = get_object_or_404(Tree, id=tid)
        initial = dict(
            tree=tree,
            contributor=request.user,
            # taxon=tree.taxon,
            lang=lang,
        )
        x, y = tree.position.coords
        x, y = transform(wgs, lambert72, x, y)
        bbox = wms_bbox(x, y, 300)
        context.update(
            dict(
                tree=tree,
                bbox=bbox,
                wms_url=wms_query(bbox),
                coords=(x, y),
                form=img_contrib_form(lang, initial=initial),
                formset=ImgContribFormSet(),
            ))

    return render(request, 'wood_core/contrib/img_contrib.html', context)


@login_required_lingua(next_name='wood-img-contrib-edit')
def img_contribution_edit(request, lang, tid, id):
    obj = get_object_or_404(ImageContribution, pk=id)

    if obj.contributor != request.user:
        return HttpResponseForbidden('user mismatch')

    context = dict(user=request.user, lang=lang, tid=tid)

    if 'POST' == request.method:
        return _post_img_contrib(request, lang, tid, context, obj)
    else:
        tree = get_object_or_404(Tree, id=tid)
        x, y = tree.position.coords
        x, y = transform(wgs, lambert72, x, y)
        bbox = wms_bbox(x, y, 300)
        context.update(
            dict(
                editing='true',
                ic=obj,
                tree=tree,
                # taxon=tree.taxon,
                bbox=bbox,
                wms_url=wms_query(bbox),
                coords=(x, y),
                form=img_contrib_form(lang, instance=obj),
            ))

    return render(request, 'wood_core/contrib/img_contrib.html', context)


@login_required
def img_delete_validation(request, lang, id):
    template_name = 'wood_core/contrib/img_delete_validation.html'
    obj = get_object_or_404(ImageContribution, pk=id)
    context = {
        'lang': lang,
        'contrib': obj,
    }
    return render(request, template_name, context)


@login_required
def img_contribution_delete(request, lang, id):
    if 'POST' != request.method:
        return HttpResponseNotAllowed(['POST'])
    contrib = get_object_or_404(ImageContribution, pk=id)
    if request.user.id != contrib.contributor.id:
        return HttpResponseForbidden('User mismatch')
    contrib.delete()

    return redirect('wood-contrib', lang)

# Text contributions


def _post_text_contrib(request, lang, tid, context, obj):
    form = text_contrib_form(lang, request.POST, instance=obj)

    if form.is_valid():
        contrib = form.save(commit=False)
        tree = get_object_or_404(Tree, id=tid)
        if tree.category == 'inv':
            tree.category = 'adpt'
        contrib.position = tree.position
        contrib.contributor = request.user
        # contrib.taxon = tree.taxon
        contrib.lang = lang

        try:
            contrib.save()
            tree.save()
            return redirect('wood-atlas-tree', lang, tid)
        except Exception as ex:
            return HttpResponseServerError(ex)

    context.update(dict(form=form))

    return render(request, 'wood_core/contrib/text_contrib.html', context)


@login_required_lingua(next_name='wood-text-contrib-new')
def text_contribution_new(request, lang, tid):
    context = dict(user=request.user, lang=lang, tid=tid)

    if 'POST' == request.method:
        return _post_text_contrib(request, lang, tid, context, None)
    else:
        tree = get_object_or_404(Tree, id=tid)
        initial = dict(
            tree=tree,
            contributor=request.user,
            # taxon=tree.taxon,
            lang=lang,
        )
        x, y = tree.position.coords
        x, y = transform(wgs, lambert72, x, y)
        bbox = wms_bbox(x, y, 300)
        context.update(
            dict(
                tree=tree,
                bbox=bbox,
                wms_url=wms_query(bbox),
                coords=(x, y),
                form=text_contrib_form(lang, initial=initial),
            ))

    return render(request, 'wood_core/contrib/text_contrib.html', context)


@login_required_lingua(next_name='wood-text-contrib-edit')
def text_contribution_edit(request, lang, tid, id):
    obj = get_object_or_404(TextContribution, pk=id)

    if obj.contributor != request.user:
        return HttpResponseForbidden('user mismatch')

    context = dict(user=request.user, lang=lang, tid=tid)

    if 'POST' == request.method:
        return _post_text_contrib(request, lang, tid, context, obj)
    else:
        tree = get_object_or_404(Tree, id=tid)
        x, y = tree.position.coords
        x, y = transform(wgs, lambert72, x, y)
        bbox = wms_bbox(x, y, 300)
        context.update(
            dict(
                tc=obj,
                tree=tree,
                bbox=bbox,
                wms_url=wms_query(bbox),
                coords=(x, y),
                form=text_contrib_form(lang, instance=obj),
            )
        )

    return render(request, 'wood_core/contrib/text_contrib.html', context)


@login_required
def text_delete_validation(request, lang, id):
    template_name = 'wood_core/contrib/text_delete_validation.html'
    obj = get_object_or_404(TextContribution, pk=id)
    tree = get_object_or_404(Tree, pk=obj.tree.id)
    context = {
        'lang': lang,
        'contrib': obj,
        'tree': tree,
    }
    return render(request, template_name, context)


@login_required
def text_contribution_delete(request, lang, id):
    if 'POST' != request.method:
        return HttpResponseNotAllowed(['POST'])
    contrib = get_object_or_404(TextContribution, pk=id)
    if request.user.id != contrib.contributor.id:
        return HttpResponseForbidden('User mismatch')
    contrib.delete()

    return redirect('wood-contrib', lang)
