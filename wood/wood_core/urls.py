from django.urls import path, include

from wood_core.views import (
    atlas_data,
    atlas_page,
    # atlas_page_contrib,
    atlas_map,
    media,
    media_site,
    about_page,
    front_page,
    list_all_trees,
    add_tree,
    edit_tree,
    # contribution_new,
    # contribution_edit,
    text_contribution_new,
    text_contribution_edit,
    text_delete_validation,
    text_contribution_delete,
    img_contribution_new,
    img_contribution_edit,
    img_contribution_delete,
    img_delete_validation,

    contribution_list,
    focus,
    # get_contrib_data,
    walks_1050
)

urlpatterns = [
    path('media-site/<name>/<int:target>', media_site, name='wood-media-site'),
    path('media/<model>/<int:id>/<int:target>', media, name='wood-media'),
    path('atlas/data/', atlas_data, name='wood-data'),
    # path('adopte/<cid>', get_contrib_data, name='wood-adopte'),
    path('<lang>/focus/<tid>', focus, name='wood-focus'),
    path('<lang>/sitemap', list_all_trees, name='list-all-trees'),

    path('<lang>/add/new', add_tree, name = 'wood-add-tree'),
    path('<lang>/edit/<int:id>', edit_tree, name = 'wood-edit-tree'),
    # path('<lang>/contrib/<int:tid>/new', contribution_new, name='wood-contrib-new'),
    # path('<lang>/contrib/edit/<int:id>',contribution_edit, name='wood-contrib-edit'),

    path('<lang>/textcontrib/<int:tid>/new', text_contribution_new, name='wood-text-contrib-new'),
    path('<lang>/textcontrib/<int:tid>/edit/<int:id>',text_contribution_edit,name='wood-text-contrib-edit'),
    path('<lang>/textcontrib/deletevalidation/<int:id>', text_delete_validation, name="wood-text-del-valid"),
    path('<lang>/textcontrib/delete/<int:id>', text_contribution_delete, name='wood-text-contrib-del'),

    path('<lang>/imgcontrib/<int:tid>/edit/<int:id>',img_contribution_edit,name='wood-img-contrib-edit'),
    path('<lang>/imgcontrib/<int:tid>/new', img_contribution_new, name='wood-img-contrib-new'),
    path('<lang>/imgcontrib/deletevalidation/<int:id>', img_delete_validation, name="wood-img-del-valid"),
    path('<lang>/imgcontrib/delete/<int:id>', img_contribution_delete, name='wood-img-contrib-del'),

    path('<lang>/contrib/', contribution_list, name='wood-contrib'),
    path('<lang>/atlas.html', atlas_map, name='wood-atlas'),
    path(
        '<lang>/atlas/id_<pid>.html',
        atlas_page,
        name='wood-atlas-tree-imported'),
    path('<lang>/atlas/<int:pid>.html', atlas_page, name='wood-atlas-tree'),
    path('1050/<lang>/', walks_1050, name='wood-walks-1050'),
    # path(
    #     '<lang>/atlas/contrib/<int:cid>.html',
    #     atlas_page_contrib,
    #     name='wood-atlas-tree-contrib'),
    path('<lang>/about.html', about_page, name='wood-about'),
    path('<lang>/index.html', front_page, name='wood-index'),
    path('<lang>/', front_page),
]
