from functools import partial
from django import template
from django.urls import reverse
from django.utils.safestring import mark_safe
from markdown import markdown
from pyproj import Proj, transform
import json

from wood_core.models.subject import Tree

wgs = Proj(init='epsg:4326')
lambert72 = Proj(init='epsg:31370')
register = template.Library()


@register.filter
def quality_score(quality):
    q = quality.quantity
    if q > 0:
        return '+' * q
    elif q < 0:
        return '-' * abs(q)

    return ''


@register.filter
def tr(message, lang):
    return getattr(message, lang, '-')

@register.filter
def lingua_url(obj, lang):
    if getattr(obj, 'get_url') is not None:
        return obj.get_url(lang)
    raise TypeError('Expected something with a get_url method, got "{}"'.format(type(obj)))

# @register.filter
# def anti_lang(lang):
#     if 'fr' == lang:
#         return 'nl'
#     return 'fr'


@register.simple_tag
def tr_label(lang, **kwargs):
    return kwargs.get(lang, '-')


@register.filter
def jumelage(tree, scope):
    return tree.jumelage_set.filter(scope=scope)


@register.filter
def embed(tree, provider):
    return tree.embedmedia_set.filter(provider=provider)


@register.filter
def md(content):
    return mark_safe(markdown(content))


def get_size(w, h, target):
    if w > h:
        r = target / w
    else:
        r = target / h
    tw = w * r
    th = h * r
    return (tw, th)


@register.filter
def width(image, target):
    return get_size(image.width, image.height, target)[0]


@register.filter
def height(image, target):
    return get_size(image.width, image.height, target)[1]


@register.filter
def ratio(image, target):
    if image.width is None:
        return ''
    w, h = get_size(image.width, image.height, target)
    if h / w * 100 < 75:
        return h / w * 100
    else:
        return 75


MEDIA_CSS_TPL = """
@media (orientation: {orientation}) and (min-width: {media_width}px) {{
   {selector} {{
    background-image: url("{url}");
  }}
}}
"""


@register.simple_tag
def media_css(selector, orientation, name, start, end):
    sizes = range(start, end, 200)
    css = []
    for sz in sizes:
        row_css = MEDIA_CSS_TPL.format(
            media_width=sz - 200,
            selector=selector,
            orientation=orientation,
            url=reverse('wood-media-site', args=(
                name,
                sz,
            )),
        )
        css.append(row_css)
    return mark_safe('\n'.join(css))


@register.filter
def imported(id):
    try:
        return Tree.objects.get(import_id='ID {}'.format(id)).id
    except Exception:
        return id


count_template = """<div class="digit-wrapper">
                        <div class="digit">{minus}</div>
                        <div class="digit">{count}</div>
                        <div class="digit">{plus}</div>
                    </div>"""


def minus(c):
    if 0 == c:
        return 9
    return c - 1


def plus(c):
    if 9 == c:
        return 0
    return c + 1


@register.filter
def counter(count):
    cs = str(count)
    res = []
    for c in cs:
        res.append(
            count_template.format(
                minus=minus(int(c)),
                count=c,
                plus=plus(int(c)),
            ))

    return mark_safe('\n'.join(res))


@register.filter
def wgs_to_lambert(pos):
    x, y = transform(wgs, lambert72, pos[0], pos[1])
    return [x, y]


@register.simple_tag
def tree_name(lang, tree, extra=''):
    has_name = True
    name = getattr(tree, 'name')
    lname = ''

    if name:
        lname = getattr(name, lang)

    if lname is '' and tree.taxon:
        has_name = False
        lname = getattr(tree.taxon.name, lang)
    
    if lname == '':
        return '*'

    if lname is None and not has_name:
        return '*'

    return '{name}{extra}'.format(name=lname, extra=extra)

@register.simple_tag
def tree_sub_title(lang, tree): #give the taxon if there is a name, and an empty string if there is no name.
    if tree.taxon:
        if getattr(tree.taxon.name, lang) == tree_name(lang, tree):
            return ''
        else:
            return getattr(tree.taxon.name, lang)
    else:
        return 'No taxon'

@register.simple_tag
def tree_description(lang, tree):
    if tree.description == None:
        return ""
    else: 
        ldesc = tr(tree.description, lang)
        if ldesc == "-" or ldesc == "":
            return ""
        else:
            return md(ldesc)

@register.simple_tag
def tree_description_title(lang, tree):
    if tree_description(lang, tree) == '':
        return ""
    else:
        return tr_label(lang, fr="Traits et caractères de l'individu", nl="Kenmerken/Karakter van het individu", en="Features and characters of the individual")

@register.filter
def to_js(obj):
    return mark_safe(json.dumps(obj))
