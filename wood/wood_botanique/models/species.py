from django.db import models
from easy_thumbnails.fields import ThumbnailerImageField

from wood_lingua.models import message_field
from wood_core.models.media import picture


class TaxonManager(models.Manager):
    def get_queryset(self):
        select = (
            'name',
            'origin',
            'soil',
            'climate',
        )
        return super().get_queryset().select_related(*select)

    def find(self, latin_name):
        genus = latin_name.split(' ')[0]
        species = ' '.join(latin_name.split(' ')[1:])

        return self.get_queryset().get(genus=genus, species=species)


class Taxon(models.Model):
    """Models a taxon
    """

    class Meta:
        ordering = ('genus', )

    id = models.AutoField(primary_key=True)
    objects = TaxonManager()

    family = models.CharField(max_length=64)
    genus = models.CharField(max_length=64)
    species = models.CharField(max_length=64)

    trivia = message_field('taxon_trivia')
    name = message_field('taxon_name')

    origin = message_field('taxon_origin')
    soil = message_field('taxon_soil')
    climate = message_field('taxon_climate')

    max_age = message_field('taxon_max_age')
    max_height = message_field('taxon_max_height')
    max_circumference = message_field('taxon_max_circumference')

    def __str__(self):
        return '{} {}'.format(self.genus, self.species)


TaxonPicture = picture(Taxon)