"use strict";

var page = require('webpage').create();
var sys = require('system');

var sources = sys.args.slice(1);

page.viewportSize = {
    width: 2000,
    height: 1900,
};

page.paperSize = {
    format: 'A4',
    orientation: 'portrait',
    // orientation: 'landscape',
    margin: '1cm',
}

// window.console.log(msg);
page.onConsoleMessage = function (msg) {
    console.log(msg);
};




function renderPage() {
    if (sources.length <= 0) {
        phantom.exit();
    }
    var source = sources.pop();
    var target = source.split('.')[0] + '.pdf';

    console.log(source, target)

    page.open(source, function (status) {
        if (status !== 'success') {
            console.error('Unable to load ', source);
            window.setTimeout(function () {
                renderPage();
            }, 1);
        }
        else {
            page.evaluate(function () {
                var styles = document.getElementsByTagName('style');
                for (var i = 0; i < styles.length; i++) {
                    var style = styles[i];
                    style.setAttribute('media', 'print');
                }

                var links = document.getElementsByTagName('a');
                var re = new RegExp('.+/target')
                var baseUrl = 'http://www.woodwideweb.be/2018';
                for (var i = 0; i < links.length; i++) {
                    var link = links[i];
                    if (link.href.slice(0, 7) === 'file://') {
                        link.href = link.href.replace(re, baseUrl);
                    }
                }
            });
            window.setTimeout(function () {
                page.render(target);
                renderPage();
            }, 1);
        }
    });
}

renderPage();
